resource "aws_secretsmanager_secret" "sm_secret" {
  name                    = local.secret_name
  recovery_window_in_days = var.sm_recovery_window_in_days
  kms_key_id              = data.aws_kms_alias.alias.target_key_id

  triggers = {
    account_id = var.account_id
    cicd_role = var.cicd_role
    active_region = var.active_region
  }
  // ignore rotation lambda and any rules that get set... the rotation process will set the lambda
  // and it races with the pipeline
  lifecycle {
    ignore_changes = [
      rotation_lambda_arn,
      rotation_rules,
    ]
  }

  provisioner "local-exec" {
    when       = destroy
    command    = "${path.module}/cleanupKeysOnDestroy.sh ${self.triggers.account_id} ${self.triggers.cicd_role} ${self.triggers.name} ${self.triggers.active_region} >> local-exec.out 2>&1 "
    on_failure = continue
    # environment = {
    #   account_id = self.triggers.account_id
    #   cicd_role = self.triggers.cicd_role
    #   active_region = self.triggers.active_region
    # }
  }

  tags = merge(
    var.default_tags,
    {
      "Name" = local.secret_name
    }
  )
}




locals {
  # Sanitize a resource name prefix:
  resource_name_prefix = replace(replace("${var.product_name}-${terraform.workspace}", "_", ""), " ", "")
  tags = {
    "Environment" = terraform.workspace
    "Product"     = lower(var.product_name)
    "TechOwner"   = var.product_tech_owner_mail
    "Owner"       = var.product_owner_mail
  }
}

resource "aws_s3_bucket" "data" {
  bucket            = "${local.resource_name_prefix}-${var.region}-data"
  force_destroy = true
  tags = merge(local.tags, {
    "Name"         = local.incoming_data_bucket_name
    "app:Region" = var.region
    "app:Profile"  = var.aws_cli_profile
  })
  provisioner "local-exec" {
    when        = destroy
    command = "python ${path.module}/scripts/clean_bucket.py -b ${self.id} -r ${self.tags["app:Region"]} -p ${self.tags["app:Profile"]}"
  }
}
